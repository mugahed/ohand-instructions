# Ohand-instructions

1. [Running 3 cells scenario.](https://gitlab.flux.utah.edu/mugahed/ohand-instructions/-/edit/main/README.md?ref_type=heads#1-install-zeromq-using)
2. [Running 3 eNB scenario.](https://gitlab.flux.utah.edu/mugahed/ohand-instructions/-/edit/main/README.md?ref_type=heads#running-3-enb-scenario)

To start Ohand first you need to follow below steps sequencially after the experiment starts:

## 1. Install ZeroMQ using:

```
sudo apt-get install libzmq3-dev
```
[Source](https://docs.srsran.com/projects/4g/en/latest/app_notes/source/zeromq/source/index.html#zeromq-appnote)
## 2. Install srsRAN 4G

- Unfortinately you can use the apt installation as the software has to check if ZeroMQ is installed during the make file checks.
- Also this make file really take long time, around 30 minutes

**To install srsRAN:**

Insall dependences:
```
sudo apt-get install build-essential cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev
```

Then, Download and build srsRAN 4G:
:
```
git clone https://github.com/srsRAN/srsRAN_4G.git
cd srsRAN_4G
mkdir build
cd build
cmake ../
make
make test
```
Then, Install srsRAN 4G:

```
sudo make install
srsran_install_configs.sh user
```
[Source](https://docs.srsran.com/projects/4g/en/latest/general/source/1_installation.html#installation-from-source)
## 3. Copy config files to the experiment

This is simply by cloning this repo to your experiment, or by any other means(sftp, etc..).

## 4. Install gnuradio to compile the GRC file
On the configration file there is a GRC file that needs to be compiled using the grcc tool from gnuradio. after compilation it produces a python file that enables the virtual radio connection between the UE and the eNB.

To install gnuradio simply run:

```
sudo apt-get install gnuradio
```
[Source](https://wiki.gnuradio.org/index.php/InstallingGR)
## 5. Compile the grc file:
Simply by running grcc on the file on the configration:
```
grcc handover_broker_3_cells.grc
```

> Side note: you can edit the grc file that describes how the eNB and the radio connects using the **gnuradio companion** software.

## 6. Run the experiment:
Note that the configration file specified below are located on this repo /conf directory, when using the below commands, use the full PATH of the files.

Start the core:
```
cd ~/srsRAN_4G/build
sudo ./srsepc/src/srsepc
```
Start the eNB:
```
cd ~/srsRAN_4G/build
sudo ./srsenb/src/srsenb --enb_files.rr_config ~/ohand-instructions/conf/rr.conf ~/ohand-instructions/conf/enb.conf
```
Start the UE and wait for it to reach "Attaching UE..." state:
```
cd ~/srsRAN_4G/build
sudo ./srsue/src/srsue ~/ohand-instructions/conf/ue.conf 
```
Start the virtual radio(ZeroMQ) using full path to the python file:
```
./intra_enb.py
```
Adjust the gain from the GUI to create the force the UE to do Handover.

> You can also skip GUI and just adjust the gain from the enb Tab using: cell_gain [cell identifier] [gain in dB]

# Installing RC xApp 

To deploy RC xApp provided by O-RAN Alliance OSC, we need to perform the following steps,

## 1. Download config and schema file:

```
wget 'https://gerrit.o-ran-sc.org/r/gitweb?p=ric-app/rc.git;a=blob_plain;f=xapp-descriptor/config.json' -O rc-config.json

wget 'https://gerrit.o-ran-sc.org/r/gitweb?p=ric-app/rc.git;a=blob_plain;f=xapp-descriptor/schema.json;hb=HEAD' -O schema-config.json
```

## 2. Onboard RC xApp:

```
sudo /local/setup/oran/dms_cli onboard rc-config.json schema-config.json
```

## 3. Deploy RC xApp: 

```
sudo /local/setup/oran/dms_cli install --xapp_chart_name=rc --version=1.0.0 --namespace=ricxapp
```

# Interacting with RC xApp through GRPC server

RC xApp exposes northbound API through a GRPC server. So, to talk with RC xApp, we need to communicate with GRPC server.  

To interact with RC xApp you, perform the following tasks,

## 1. Install GRPCURL:

```
curl -sSL "https://github.com/fullstorydev/grpcurl/releases/download/v1.8.7/grpcurl_1.8.7_linux_x86_64.tar.gz" | sudo tar -xz -C /usr/local/bin
```

## 2. Setting the GRPC server IP and port:

To set the GRPC service IP and port, run the following command:

```
export GRPC_IP=`kubectl get services -n ricxapp | grep 'service-ricxapp-rc-grpc-server' | awk '{print $3}'` GRPC_PORT=`kubectl get services -n ricxapp | grep 'service-ricxapp-rc-grpc-server' | awk '{print $5}' | awk -F'/' '{print $1}'`
```

## 3. Sending JSON string to RC xApp:

```
grpcurl -plaintext -d "{ \"e2NodeID\": \"enB_macro_901_070_00019b\", \"plmnID\": \"61712\", \"ranName\": \"enB_macro_901_070_00019b\", \"RICE2APHeaderData\": { \"RanFuncId\": 3, \"RICRequestorID\": 2 }, \"RICControlHeaderData\": { \"ControlStyle\": 3, \"ControlActionId\": 1, \"UEID\": {\"GnbUEID\": { \"amfUENGAPID\": 10 , \"guami\": {\"pLMNIdentity\": \"111111\",\"aMFRegionID\": \"00001001\", \"aMFSetID\": \"000000 \", \"aMFPointer\": \" 000\"  },\"gNBCUUEF1APID\": 2, \"gNBCUCPUEE1APID\": 1 }} }, \"RICControlMessageData\": { \"RICControlCellTypeVal\": 4, \"TargetCellID\": \"1113\" }, \"RICControlAckReqVal\": 1 }" ${GRPC_IP}:${GRPC_PORT} rc.MsgComm.SendRICControlReqServiceGrpc
```

You will see a success response if every step goes right.

# Setting up srsLTE with E2 Agent:

# 1. Clone repository:

```
git clone https://gitlab.flux.utah.edu/mali3119/srslte-rc-ric
```

# 2. Compile the srsLTE:

```
cd srslte-rc-ric; mkdir build; sudo ./build_srslte.sh
```

# 3. Run srsLTE:

```
. /local/repository/demo/get-env.sh

sudo build/srsenb/src/srsenb \
    --enb.n_prb=15 --enb.name=enb1 --enb.enb_id=0x19B --rf.device_name=zmq \
    --rf.device_args="fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001" \
    --ric.agent.remote_ipv4_addr=${E2TERM_SCTP} \
    --ric.agent.local_ipv4_addr=10.10.1.1 --ric.agent.local_port=52525 \
    --log.all_level=warn --ric.agent.log_level=debug --log.filename=stdout \
    --slicer.enable=1 --slicer.workshare=0
```


# Running 3 eNB scenario

S1 handover is not supported by srsepc so you need to install any alternative. in srsran example they use open5gs.

## 1. installing open5gs
This is a straigh forward from official [documentation](https://open5gs.org/open5gs/docs/guide/01-quickstart/). make sure you install the webUI so you can add srsran subscriber later on.

**VERY IMPORTANT**: when starting the experiment don't use server d710 or d430, as they use Intel xeon cpu, which does not supports Advanced Vector Extensions required by mongoDB.
Alternativly you can use d840.


**VERY IMPORTANT**: to make open5gs accessable from public ip of the machine add `Environment=HOSTNAME=0.0.0.0` to `/lib/systemd/system/open5gs-webui.service`. then do:

`sudo systemctl stop  open5gs-webui.service`

 & 

`sudo systemctl start open5gs-webui.service`

sometimes when doing the above _stop_ command you might face issue like below:
` Warning: The unit file, source configuration file or drop-ins of open5gs-webui.service changed on disk. Run 'systemctl daemon-reload' to reload units.
`

to solve it just run `sudo systemctl daemon-reload`
<details><summary>Create dummy interfaces to connect additoinal eNBs</summary>


To create a dummy2 interfaces for additional eNBs connection to E2 agent, follow these steps:


1. **Create the dummy2 interface:**

```bash
sudo ip link add dummy2 type dummy
```

2. **Bring the dummy2 interface up:**

```bash
sudo ip link set dummy2 up
```

3. **Assign an IP address to the dummy2 interface:**

```bash
sudo ip address add 10.10.2.2/24 dev dummy2
```

4. **Verify that the dummy2 interface is up and running:**

```bash
sudo ip link show dummy2
```

You should see the output of the following command:

```
dummy2: flags=0x10000<UP,BROADCAST,RUNNING,MULTICAST> mtu=1500
    loopback,offlink_txq,offlink_rxq txq_flags=0x00000000
    rxq_flags=0x00000000
```

This indicates that the dummy2 interface is up and running.

6. **To remove the dummy2 interface:**

```bash
sudo ip link del dummy2
```

This will remove the dummy2 interface and all of its associated configuration.

</details>

## 2. open GUI and install the UE.
use the server public ip and use port 3000, username: admin, password: 1423, follow [this link](https://docs.srsran.com/projects/4g/en/latest/app_notes/source/handover/source/index.html#id1) for the configration

## 3. stop and start open5gs
copy the correct mme configuration:
 
``` 
sudo cp ohand-instructions/conf/s1Handover/mme.yaml /etc/open5gs/mme.yaml
```
then stop and start
```
sudo systemctl stop open5gs-mmed
sudo systemctl stop open5gs-sgwcd
sudo systemctl stop open5gs-smfd
sudo systemctl stop open5gs-amfd
sudo systemctl stop open5gs-sgwud
sudo systemctl stop open5gs-upfd
sudo systemctl stop open5gs-hssd
sudo systemctl stop open5gs-pcrfd
sudo systemctl stop open5gs-nrfd
sudo systemctl stop open5gs-scpd
sudo systemctl stop open5gs-seppd
sudo systemctl stop open5gs-ausfd
sudo systemctl stop open5gs-udmd
sudo systemctl stop open5gs-pcfd
sudo systemctl stop open5gs-nssfd
sudo systemctl stop open5gs-bsfd
sudo systemctl stop open5gs-udrd
sudo systemctl stop open5gs-webui
```
```
sudo systemctl start open5gs-mmed
sudo systemctl start open5gs-sgwcd
sudo systemctl start open5gs-smfd
sudo systemctl start open5gs-amfd
sudo systemctl start open5gs-sgwud
sudo systemctl start open5gs-upfd
sudo systemctl start open5gs-hssd
sudo systemctl start open5gs-pcrfd
sudo systemctl start open5gs-nrfd
sudo systemctl start open5gs-scpd
sudo systemctl start open5gs-seppd
sudo systemctl start open5gs-ausfd
sudo systemctl start open5gs-udmd
sudo systemctl start open5gs-pcfd
sudo systemctl start open5gs-nssfd
sudo systemctl start open5gs-bsfd
sudo systemctl start open5gs-udrd
sudo systemctl start open5gs-webui
```

## 4. run the 2 enb s1handover

1.  create a dummy ip for the second eNB using the instructions above.

2.  use below to start the first eNB:
```
sudo /local/setup/srslte-ric/build/srsenb/src/srsenb --enb.n_prb=15 --enb.name=enb1 --enb.enb_id=0x19B --rf.device_name=zmq --rf.device_args="fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001" --ric.agent.remote_ipv4_addr=${E2TERM_SCTP} --ric.agent.local_ipv4_addr=10.10.1.1 --ric.agent.local_port=52525 --log.all_level=warn --ric.agent.log_level=debug --log.filename=stdout --slicer.enable=1 --slicer.workshare=0          
```
4. use below to start the second eNB:
```
sudo /local/setup/srslte-ric/build/srsenb/src/srsenb --enb.n_prb=15 --enb.name=enb1 --enb.enb_id=0x19B --rf.device_name=zmq --rf.device_args="fail_on_disconnect=true,id=enb,base_srate=23.04e6,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001" --ric.agent.remote_ipv4_addr=${E2TERM_SCTP} --ric.agent.local_ipv4_addr=10.10.2.2 --ric.agent.local_port=52525 --log.all_level=warn --ric.agent.log_level=debug --log.filename=stdout --slicer.enable=1 --slicer.workshare=0          
```

5. run the UE: 
use:
```
ohand-instructions/s1Handover/ue 
```
6. compile the grc file and run the generated python file:
```
grcc ohand-instructions/s1Handover/handover_broker_eNB.grc

```

## 5. Steps to run the experiment

Follow these steps to execute the experiment
1. Start [O-RAN](https://www.powderwireless.net/show-profile.php?uuid=30d3e13c-f939-11ea-b1eb-e4434b2381fc) powder profile. 
2. [Install](#installing-rc-xapp) RAN control xApp.
3. Install [Open5GS](#running-3-enb-scenario).
4. enter webUI using `servel_public_IP:3000` and add new UE using the configuration found on `ohand-instructions/conf/ue.conf` line 140
5. Execute the following commands:

```
python3 -m pip install packaging

sudo add-apt-repository ppa:gnuradio/gnuradio-releases
sudo apt-get update
sudo apt-get install gnuradio python3-packaging
```

6. Clone [srs-lte-ric](https://gitlab.flux.utah.edu/mali3119/srslte-rc-ric) in the user's home directory. Navigate to the cloned directory and create a `build` folder. After it, execute `sudo ./build_srsran.sh`.
7. Clone this repository, open three terminals/tmux tabs and navigate to `ohand-instructions/conf/s1Handover` in all of them.
8. Run the following commands in any of the terminal.
```
chmod 755 enb1_ric
chmod 755 enb2_ric 
chmod 755 enb3_ric
chmod ue
```
9. Run `. /local/repository/demo/get-env.sh` in all three tabs.
10. Execute `sudo ./enb1_ric` in tab1, `sudo ./enb2_ric` in tab2 and `sudo ./enb3_ric` in tab3. 
11. Open another (4th) terminal/tmux tab and navigate to `ohand-instructions/conf/s1Handover`.
12. Execute `sudo ./ue`
13. Open 5th terminal/tmux tab and navigate to `ohand-instructions/conf/s1Handover/3cellout`. Then execute `python3 intra_gnb.py`.
14. Open 6th terminal/tmux tab and execute [these](#interacting-with-rc-xapp-through-grpc-server) instructions to send JSON string through xApp's northbound API. However, before sending the JSON string, change `TargetCellID` parameter from `1113` to `19D01`. 
15. Execute the Handover from cell0 with PCI: 1 (cell_gain0) to cell1 with PCI: 6 (cell_gain1). However, the UE's traces would show that it is synchronized with PCI: 8 (cell_gain2). The reason is we overwrote the target ECI value in handover :\). 
